package com.mat_brandao.omdb.domain.util

/**
 * Created by Mateus Brandão on 22/06/2016.
 */
interface GenericObjectClickListener<T> {
    fun onItemClick(`object`: T)
}
