package com.mat_brandao.omdb.domain.util

enum class NetworkState {
    SUCCESS,
    ERROR,
    LOADING
}