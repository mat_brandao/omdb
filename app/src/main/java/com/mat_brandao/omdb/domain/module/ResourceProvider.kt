package com.mat_brandao.omdb.domain.module

import android.content.Context

open class ResourceProvider(val context: Context) {

    fun getString(resId: Int): String {
        return context.getString(resId)
    }

    fun getString(resId: Int, formatArgs: Long): String {
        return context.getString(resId, formatArgs)
    }
}