package com.mat_brandao.omdb.domain.work

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.media.RingtoneManager
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.mat_brandao.omdb.R
import com.mat_brandao.omdb.domain.util.IntentValues.INTENT_MOVIE_ID_KEY
import com.mat_brandao.omdb.domain.util.IntentValues.INTENT_MOVIE_TITLE_KEY
import com.mat_brandao.omdb.view.movie_detail.MovieDetailActivity

class AlarmWorker(private val context: Context, workerParams: WorkerParameters): Worker(context, workerParams) {


    override fun doWork(): Result {
        val movieTitle = inputData.getString(INTENT_MOVIE_TITLE_KEY)
        val movieId = inputData.getString(INTENT_MOVIE_ID_KEY)

        sendNotification("Estréia de $movieTitle",
                "Não perca a estréia de $movieTitle nos cinemas!", movieId!!)

        return Result.success()
    }

    private fun sendNotification(title: String, messageBody: String, movieId: String) {
        val intent = Intent(context, MovieDetailActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        intent.putExtra(INTENT_MOVIE_ID_KEY, movieId)
        val pendingIntent = PendingIntent.getActivity(context, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT)

        val myLogo = BitmapFactory.decodeResource(context.resources, R.mipmap.ic_launcher_round)

        val channelId = "Notificações"
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder = NotificationCompat.Builder(context, channelId)
                .setSmallIcon(R.mipmap.ic_launcher_round)
                .setLargeIcon(myLogo)
                .setContentTitle(title)
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent)

        val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(channelId,
                    "Notificações",
                    NotificationManager.IMPORTANCE_DEFAULT)
            notificationManager.createNotificationChannel(channel)
        }

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build())
    }

}