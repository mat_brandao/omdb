package com.mat_brandao.omdb.domain.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
data class Rating(
        @JsonProperty("Source") var source: String? = null,
        @JsonProperty("Value") var value: String? = null)