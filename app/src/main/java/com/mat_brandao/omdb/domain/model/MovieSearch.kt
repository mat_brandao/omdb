package com.mat_brandao.omdb.domain.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty

import java.io.Serializable

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
data class MovieSearch(
        @JsonProperty("Search") var movieList: MutableList<Movie>? = null,
        @JsonProperty("totalResults") var totalResults: Int = 0,
        @JsonProperty("Response") var isResponse: Boolean = false,
        @JsonProperty("Error") var error: String? = null
) : Serializable
