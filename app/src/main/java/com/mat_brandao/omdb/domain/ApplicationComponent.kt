package com.mat_brandao.omdb.domain

import com.mat_brandao.omdb.domain.module.ApiModule
import com.mat_brandao.omdb.domain.module.ApplicationModule
import com.mat_brandao.omdb.view.App
import com.mat_brandao.omdb.view.movie_detail.MovieDetailViewModel
import com.mat_brandao.omdb.view.movie_search.MovieSearchViewModel
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [
    ApplicationModule::class,
    ApiModule::class])
interface ApplicationComponent {
    fun inject(viewModel: MovieSearchViewModel)
    fun inject(viewModel: MovieDetailViewModel)
    fun inject(app: App)

    interface Injectable {
        fun inject(applicationComponent: ApplicationComponent)
    }
}