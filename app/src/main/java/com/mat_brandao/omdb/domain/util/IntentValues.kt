package com.mat_brandao.omdb.domain.util

object IntentValues {
    val INTENT_MOVIE_DATA_KEY = "intent_movie_data"
    val INTENT_MOVIE_ID_KEY = "intent_movie_id"
    val INTENT_MOVIE_TITLE_KEY = "intent_movie_title"
}
