package com.mat_brandao.omdb.domain.module

import android.content.Context
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
open class ApplicationModule(private val context: Context) {

    @Singleton
    @Provides
    open fun provideContext(): Context = context

    @Singleton
    @Provides
    open fun getResourceProvider(): ResourceProvider = ResourceProvider(context)

}