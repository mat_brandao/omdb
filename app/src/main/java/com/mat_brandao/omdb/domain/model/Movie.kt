package com.mat_brandao.omdb.domain.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty

import java.io.Serializable

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
data class Movie(
        @JsonProperty("Title") var title: String? = null,
        @JsonProperty("Year") var year: String? = null,
        @JsonProperty("Rated") var rated: String? = null,
        @JsonProperty("Released") var released: String? = null,
        @JsonProperty("Runtime") var runtime: String? = null,
        @JsonProperty("Genre") var genre: String? = null,
        @JsonProperty("Director") var director: String? = null,
        @JsonProperty("Writer") var writer: String? = null,
        @JsonProperty("Actors") var actors: String? = null,
        @JsonProperty("Plot") var plot: String? = null,
        @JsonProperty("Language") var language: String? = null,
        @JsonProperty("Country") var country: String? = null,
        @JsonProperty("Awards") var awards: String? = null,
        @JsonProperty("Poster") var poster: String? = null,
        @JsonProperty("Ratings") var ratings: List<Rating>? = null,
        @JsonProperty("Metascore") var metascore: String? = null,
        @JsonProperty("imdbRating") var imdbRating: String? = null,
        @JsonProperty("imdbVotes") var imdbVotes: String? = null,
        @JsonProperty("imdbID") var imdbID: String? = null,
        @JsonProperty("Type") var type: String? = null,
        @JsonProperty("DVD") var dvd: String? = null,
        @JsonProperty("BoxOffice") var boxOffice: String? = null,
        @JsonProperty("Production") var production: String? = null,
        @JsonProperty("Website") var website: String? = null,
        @JsonProperty("Response") var isResponse: Boolean = false,
        @JsonProperty("Error") var error: String? = null

) : Serializable
