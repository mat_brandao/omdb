package com.mat_brandao.omdb.domain.util

/**
 * Created by Mateus Brandão on 28/07/2016.
 */
interface GenericActionListener {
    fun onAction()
}
