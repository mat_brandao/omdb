package com.mat_brandao.omdb.domain.arch;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DisposableLiveData {
    private Map<LiveData, List<Observer>> dataMap = new HashMap<>();

    public <T> void add(LiveData<T> liveData, Observer<T> observer) {
        liveData.observeForever(observer);
        if (!dataMap.containsKey(liveData)) {
            dataMap.put(liveData, new ArrayList<>());
        }
        dataMap.get(liveData).add(observer);
    }

    public void dispose(LiveData liveData) {
        if (dataMap.containsKey(liveData)) {
            for (Observer observer : dataMap.get(liveData)) {
                liveData.removeObserver(observer);
            }
        }
    }

    public void dispose() {
        for (LiveData liveData : dataMap.keySet()) {
            for (Observer observer : dataMap.get(liveData)) {
                liveData.removeObserver(observer);
            }
        }
    }
}
