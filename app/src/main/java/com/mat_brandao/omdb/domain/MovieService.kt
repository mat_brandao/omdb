package com.mat_brandao.omdb.domain

import com.mat_brandao.omdb.domain.model.Movie
import com.mat_brandao.omdb.domain.model.MovieSearch

import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Service to handle product related endpoints
 */
interface MovieService {
    @GET("/")
    fun searchMovieList(@Query("page") page: Long?, @Query("s") term: String,
                        @Query("type") type: String, @Query("apikey") apiKey: String): Observable<Response<MovieSearch>>

    @GET("/")
    fun getMovieInformation(@Query("i") id: String, @Query("apikey") apiKey: String): Observable<Response<Movie>>
}
