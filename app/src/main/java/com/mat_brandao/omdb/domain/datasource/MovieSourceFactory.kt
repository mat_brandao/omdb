package com.mat_brandao.omdb.domain.datasource

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.mat_brandao.omdb.domain.MovieService
import com.mat_brandao.omdb.domain.model.Movie
import com.mat_brandao.omdb.domain.util.NetworkState
import io.reactivex.disposables.CompositeDisposable

class MovieSourceFactory(private val compositeDisposable: CompositeDisposable,
                         private val movieService: MovieService,
                         private val networkIsLoading: MutableLiveData<NetworkState>,
                         private val term: String) : DataSource.Factory<Long, Movie>() {

    var usersDataSourceLiveData = MutableLiveData<MovieDataRepository>()
    private var movieDataRepository: MovieDataRepository? = null

    override fun create(): DataSource<Long, Movie> {
        movieDataRepository = MovieDataRepository(compositeDisposable, movieService, networkIsLoading, term)
        usersDataSourceLiveData.postValue(movieDataRepository)
        return movieDataRepository as MovieDataRepository
    }
}
