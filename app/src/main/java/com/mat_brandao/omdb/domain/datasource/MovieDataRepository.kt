package com.mat_brandao.omdb.domain.datasource

import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import com.mat_brandao.omdb.domain.MovieService
import com.mat_brandao.omdb.domain.model.Movie
import com.mat_brandao.omdb.domain.util.NetworkState
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import java.util.*

class MovieDataRepository(private val compositeDisposable: CompositeDisposable,
                          private val service: MovieService,
                          private val networkIsLoading: MutableLiveData<NetworkState>,
                          private val term: String) : PageKeyedDataSource<Long, Movie>() {
    private var totalPageCount: Int = 0

    override fun loadInitial(params: LoadInitialParams<Long>, callback: LoadInitialCallback<Long, Movie>) {
        networkIsLoading.postValue(NetworkState.LOADING)
        compositeDisposable.add(service.searchMovieList(1L, term, "movie", OMDB_API_KEY)
                .subscribe({ response ->
                    if (response.isSuccessful) {
                        totalPageCount = response.body()!!.totalResults / 10

                        if (response.body()!!.movieList != null) {
                            response.body()!!.movieList!!.sortWith(compareByDescending { movie -> movie.year })
                            callback.onResult(response.body()!!.movieList!!, 0L, if (2L > totalPageCount) null else 2L)
                        } else {
                            callback.onResult(ArrayList(), 0L, if (2L > totalPageCount) null else 2L)
                        }
                        networkIsLoading.postValue(NetworkState.SUCCESS)
                    } else {
                        networkIsLoading.postValue(NetworkState.ERROR)
                    }
                }, { error ->
                    error.printStackTrace()
                    networkIsLoading.postValue(NetworkState.ERROR)
                }))
    }

    override fun loadBefore(params: LoadParams<Long>, callback: LoadCallback<Long, Movie>) {}

    override fun loadAfter(params: LoadParams<Long>, callback: LoadCallback<Long, Movie>) {
        networkIsLoading.postValue(NetworkState.LOADING)
        compositeDisposable.add(service.searchMovieList(params.key, term, "movie", OMDB_API_KEY)
                .subscribe({ response ->
                    if (response.isSuccessful) {
                        callback.onResult(response.body()!!.movieList!!, if (params.key + 1 > totalPageCount) null else params.key + 1)
                        networkIsLoading.postValue(NetworkState.SUCCESS)
                    } else {
                        networkIsLoading.postValue(NetworkState.ERROR)
                    }
                }, { error -> networkIsLoading.postValue(NetworkState.ERROR) }))
    }

    fun fetchMovieFullData(): Observable<Movie> {
        return Observable.create { e ->
            compositeDisposable.add(service.getMovieInformation(term, OMDB_API_KEY)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({ movieResponse ->
                        if (movieResponse.isSuccessful && movieResponse.body()!!.isResponse) {
                            e.onNext(movieResponse.body()!!)
                        } else {
                            e.onError(EmptyBodyException())
                        }
                    }, { e.onError(it) }))
        }
    }

    inner class EmptyBodyException : Exception()

    companion object {
        val OMDB_API_KEY = "721dc598"
    }
}
