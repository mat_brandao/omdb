package com.mat_brandao.omdb.view.movie_search

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.mat_brandao.omdb.domain.ApplicationComponent
import com.mat_brandao.omdb.domain.MovieService
import com.mat_brandao.omdb.domain.arch.DisposableLiveData
import com.mat_brandao.omdb.domain.datasource.MovieSourceFactory
import com.mat_brandao.omdb.domain.model.Movie
import com.mat_brandao.omdb.domain.util.NetworkState
import com.mat_brandao.omdb.view.App
import io.reactivex.disposables.CompositeDisposable
import java.util.*
import javax.inject.Inject

class MovieSearchViewModel : ViewModel(), ApplicationComponent.Injectable {

    @Inject
    lateinit var movieService: MovieService

    private val compositeDisposable = CompositeDisposable()
    private val disposableLiveData = DisposableLiveData()
    var movieList = MutableLiveData<PagedList<Movie>>()
    var networkState = MutableLiveData<NetworkState>()

    private var config: PagedList.Config? = null

    var featuredList: List<Movie> = object : ArrayList<Movie>() {
        init {
            add(Movie(imdbID = "tt6663582", title = "The Spy Who Dumped Me", poster = "https://m.media-amazon.com/images/M/MV5BNDY1MTA0NjgyN15BMl5BanBnXkFtZTgwMTEzNDQ4NTM@._V1_SX300.jpg", imdbRating = "6.4"))
            add(Movie(imdbID = "tt4575576", title = "Christopher Robin", poster = "https://m.media-amazon.com/images/M/MV5BMjAzOTM2OTAyNF5BMl5BanBnXkFtZTgwNTg5ODg1NTM@._V1_SX300.jpg", imdbRating = "7.8"))
            add(Movie(imdbID = "tt4912910", title = "Mission: Impossible - Fallout", poster = "https://m.media-amazon.com/images/M/MV5BMTk3NDY5MTU0NV5BMl5BanBnXkFtZTgwNDI3MDE1NTM@._V1_SX300.jpg", imdbRating = "8.4"))
            add(Movie(imdbID = "tt4244998", title = "Alpha", poster = "https://m.media-amazon.com/images/M/MV5BODI4OTk1ODY3N15BMl5BanBnXkFtZTgwMDI1MTcwNjM@._V1_SX300.jpg", imdbRating = "7.0"))
            add(Movie(imdbID = "tt4560436", title = "Mile 22", poster = "https://m.media-amazon.com/images/M/MV5BNzUyODk4OTkxNF5BMl5BanBnXkFtZTgwMzY0MDgzNTM@._V1_SX300.jpg", imdbRating = "6.1"))
            add(Movie(imdbID = "tt6911608", title = "Mamma Mia! Here We Go Again", poster = "https://m.media-amazon.com/images/M/MV5BMjEwMTM3OTI1NV5BMl5BanBnXkFtZTgwNDk5NTY0NTM@._V1_SX300.jpg", imdbRating = "7.2"))
            add(Movie(imdbID = "tt5690360", title = "Slender Man", poster = "https://m.media-amazon.com/images/M/MV5BMjE0MzcwMDAyNl5BMl5BanBnXkFtZTgwMzc4ODg0NDM@._V1_SX300.jpg", imdbRating = "2.9"))
        }
    }
    private var factory: MovieSourceFactory? = null

    override fun inject(applicationComponent: ApplicationComponent) {
        App.instance!!.applicationComponent.inject(this)

        config = PagedList.Config.Builder()
                .setPageSize(pageCount)
                .setInitialLoadSizeHint(pageCount * 2)
                .setEnablePlaceholders(false)
                .build()
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
        disposableLiveData.dispose()
    }

    fun fetchMovies(term: String) {
        factory = MovieSourceFactory(compositeDisposable, movieService, networkState, term)
        disposableLiveData.add(LivePagedListBuilder(factory!!, config!!).build()) { movies ->
            movieList.postValue(movies)
        }
    }

    companion object {
        private val TAG = "MovieSearchViewModel"
        var pageCount = 10
    }
}
