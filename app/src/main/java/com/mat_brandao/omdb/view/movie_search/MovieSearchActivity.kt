package com.mat_brandao.omdb.view.movie_search

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.Menu
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.viewpager.widget.ViewPager
import com.jakewharton.rxbinding2.support.v7.widget.RxSearchView
import com.mat_brandao.omdb.R
import com.mat_brandao.omdb.domain.model.Movie
import com.mat_brandao.omdb.domain.util.GenericObjectClickListener
import com.mat_brandao.omdb.domain.util.IntentValues.INTENT_MOVIE_DATA_KEY
import com.mat_brandao.omdb.domain.util.NetworkState
import com.mat_brandao.omdb.view.App
import com.mat_brandao.omdb.view.ViewModelFactory
import com.mat_brandao.omdb.view.movie_detail.MovieDetailActivity
import com.squareup.picasso.Callback
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.activity_movie_search.*
import java.util.*
import java.util.concurrent.TimeUnit

class MovieSearchActivity : AppCompatActivity(), GenericObjectClickListener<Movie>, Callback {

    private lateinit var viewModel: MovieSearchViewModel

    private var currentPage = 0
    private var timer: Timer? = null
    private val handler = Handler()
    private val DELAY_MS: Long = 3000
    private val PERIOD_MS: Long = 3000
    private var searchView: SearchView? = null

    private lateinit var movieAdapter: MovieAdapter

    internal val update = {
        if (featured_view_pager.adapter != null) {
            if (currentPage == featured_view_pager.adapter!!.count) {
                currentPage = 0
            }
            featured_view_pager.setCurrentItem(currentPage++, true)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_search)

        setSupportActionBar(toolbar)

        viewModel = ViewModelProviders.of(this,
                ViewModelFactory(application as App))
                .get(MovieSearchViewModel::class.java)

        featured_view_pager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
                pauseTimer()
                startTimer()
            }

            override fun onPageSelected(position: Int) {}

            override fun onPageScrollStateChanged(state: Int) {}
        })

        featured_view_pager.adapter = FeaturedMovieAdapter(this, viewModel.featuredList, this, this)

        movie_recycler.layoutManager = LinearLayoutManager(this)
        movie_recycler.setHasFixedSize(true)

        movieAdapter = MovieAdapter(this)

        movie_recycler.adapter = movieAdapter

        viewModel.movieList
                .observe(this, Observer {
                    if (it.size > 0) {
                        movieAdapter.setLoading(false)
                        movieAdapter.submitList(it)
                        empty_view.visibility = View.GONE
                    } else {
                        if (viewModel.networkState.value == NetworkState.SUCCESS) {
                            Toast.makeText(this, "Não foram encontrados filmes para esta busca.", Toast.LENGTH_SHORT).show()
                            empty_view.visibility = View.VISIBLE
                        }
                    }
                })

        viewModel.networkState
                .observe(this, Observer {
                    when (it) {
                        NetworkState.SUCCESS -> if (movieAdapter.itemCount > 0) {
                            movieAdapter.setLoading(false)
                        } else {
                            error_view.visibility = View.GONE
                            loading_view.visibility = View.GONE
                        }
                        NetworkState.LOADING -> if (movieAdapter.itemCount > 0) {
                            movieAdapter.setLoading(true)
                        } else {
                            loading_view.visibility = View.VISIBLE
                            empty_view.visibility = View.GONE
                            error_view.visibility = View.GONE
                        }
                        NetworkState.ERROR -> if (movieAdapter.itemCount > 0) {
                            movieAdapter.setLoading(false)
                        } else {
                            loading_view.visibility = View.GONE
                            empty_view.visibility = View.GONE
                            error_view.visibility = View.VISIBLE
                        }
                    }
                })
    }

    override fun onItemClick(movie: Movie) {
        val intent = Intent(this, MovieDetailActivity::class.java)
        intent.putExtra(INTENT_MOVIE_DATA_KEY, movie)
        startActivity(intent)
    }

    @SuppressLint("CheckResult")
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.search_menu, menu)

        val mSearch = menu.findItem(R.id.action_search)

        searchView = mSearch.actionView as SearchView
        searchView!!.queryHint = "Buscar filmes"
        searchView!!.setOnCloseListener {
            movieAdapter.setLoading(false)
            movieAdapter.submitList(null)
            empty_view.visibility = View.VISIBLE
            false
        }

        RxSearchView.queryTextChanges(searchView!!)
                .debounce(500, TimeUnit.MILLISECONDS)
                .filter { charSequence -> charSequence.length > 2 }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { term ->
                    if (term.toString().isNotEmpty()) {
                        viewModel.fetchMovies(term.toString())
                    } else {
                        movieAdapter.setLoading(false)
                        movieAdapter.submitList(null)
                        empty_view.visibility = View.VISIBLE
                    }
                }

        return super.onCreateOptionsMenu(menu)
    }

    override fun onSuccess() {
        featured_loading_bar.visibility = View.GONE
    }

    override fun onError(e: Exception) {
        e.printStackTrace()
    }

    fun startTimer() {
        timer = Timer()
        timer!!.schedule(object : TimerTask() {
            override fun run() {
                handler.post(update)
            }
        }, DELAY_MS, PERIOD_MS)
    }

    fun pauseTimer() {
        if (timer != null) {
            timer!!.cancel()
        }
    }

    fun hideKeyboard() {
        val imm = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(searchView!!.windowToken, 0)
    }
}
