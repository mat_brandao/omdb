package com.mat_brandao.omdb.view.common

data class ReminderAction(
        val isWorkEnqueued: Boolean,
        val movieTitle: String)