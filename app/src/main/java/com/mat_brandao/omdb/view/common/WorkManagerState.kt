package com.mat_brandao.omdb.view.common

import androidx.work.WorkInfo

sealed class WorkManagerState {

    data class Subscribe(val imdbId: String): WorkManagerState()
    data class WorkInfoState(val workState: WorkInfo.State?): WorkManagerState()

    companion object {
        fun subscribe(imdbId: String): WorkManagerState =
                Subscribe(imdbId)

        fun workInfoState(workState: WorkInfo.State?): WorkManagerState =
                workInfoState(workState)
    }
}