package com.mat_brandao.omdb.view.splash

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import butterknife.ButterKnife
import com.mat_brandao.omdb.R
import com.mat_brandao.omdb.view.movie_search.MovieSearchActivity
import kotlinx.android.synthetic.main.activity_splash.*


class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        ButterKnife.bind(this)

        logo_img.animate()
                .alpha(1f)
                .setDuration(700)
                .start()

        logo_img.animate()
                .scaleX(2.5f)
                .scaleY(2.5f)
                .setDuration(1200)
                .start()

        Handler().postDelayed({
            startActivity(Intent(this, MovieSearchActivity::class.java))
            finish()
        }, 1200)
    }
}
