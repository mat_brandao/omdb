package com.mat_brandao.omdb.view

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

import com.mat_brandao.omdb.domain.ApplicationComponent

class ViewModelFactory(private val application: App) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        val t = super.create(modelClass)
        if (t is ApplicationComponent.Injectable) {
            (t as ApplicationComponent.Injectable).inject(application.applicationComponent)
        }
        return t
    }
}
