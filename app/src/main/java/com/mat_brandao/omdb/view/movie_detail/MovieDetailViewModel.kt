package com.mat_brandao.omdb.view.movie_detail

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.work.*
import com.mat_brandao.omdb.R
import com.mat_brandao.omdb.domain.ApplicationComponent
import com.mat_brandao.omdb.domain.MovieService
import com.mat_brandao.omdb.domain.arch.DisposableLiveData
import com.mat_brandao.omdb.domain.datasource.MovieDataRepository
import com.mat_brandao.omdb.domain.model.Movie
import com.mat_brandao.omdb.domain.module.ResourceProvider
import com.mat_brandao.omdb.domain.util.IntentValues.INTENT_MOVIE_ID_KEY
import com.mat_brandao.omdb.domain.util.IntentValues.INTENT_MOVIE_TITLE_KEY
import com.mat_brandao.omdb.domain.util.NetworkState
import com.mat_brandao.omdb.domain.util.SingleLiveEvent
import com.mat_brandao.omdb.domain.work.AlarmWorker
import com.mat_brandao.omdb.view.App
import com.mat_brandao.omdb.view.common.ReminderAction
import com.mat_brandao.omdb.view.common.WorkManagerState
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class MovieDetailViewModel : ViewModel(), ApplicationComponent.Injectable {

    @Inject
    lateinit var movieService: MovieService
    @Inject
    lateinit var resourceProvider: ResourceProvider

    private val compositeDisposable = CompositeDisposable()
    private val disposableLiveData = DisposableLiveData()

    var movieData = MutableLiveData<Movie>()
    var networkState = MutableLiveData<NetworkState>()
    val workManagerState: SingleLiveEvent<WorkManagerState> = SingleLiveEvent()
    val reminderAction: SingleLiveEvent<ReminderAction> = SingleLiveEvent()
    val toastEvent: SingleLiveEvent<String> = SingleLiveEvent()

    private var movieDataRepository: MovieDataRepository? = null

    var isWorkEnqueued = false

    override fun inject(applicationComponent: ApplicationComponent) {
        App.instance!!.applicationComponent.inject(this)
    }

    fun setupMovieLiveData(movie: Movie) {
        movieData.value = movie

        movieDataRepository = MovieDataRepository(compositeDisposable, movieService, networkState, movie.imdbID!!)
        networkState.value = NetworkState.LOADING
        compositeDisposable.add(movieDataRepository!!.fetchMovieFullData()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ value ->
                    networkState.value = NetworkState.SUCCESS
                    movieData.setValue(value)

                    value.released?.let {
                        if (shouldShowMenu(value)) {
                            workManagerState.postValue(WorkManagerState.Subscribe(value.imdbID!!))
                        }
                    }
                }, {
                    it.printStackTrace()
                    networkState.setValue(NetworkState.ERROR)
                }))
    }

    fun onMenuReminderClick() {
        reminderAction.postValue(ReminderAction(isWorkEnqueued, movieData.value!!.title!!))
    }

    fun onWorkEventReceived(workInfo: List<WorkInfo>) {
        if (workInfo.isNotEmpty() && workInfo[0].state == WorkInfo.State.ENQUEUED) {
            isWorkEnqueued = true
            workManagerState.postValue(WorkManagerState.WorkInfoState(WorkInfo.State.ENQUEUED))
        } else {
            workManagerState.postValue(WorkManagerState.WorkInfoState(null))
        }
    }

    fun cancelScheduledReminderWork() {
        WorkManager.getInstance().cancelAllWorkByTag(movieData.value!!.imdbID!!)
        toastEvent.postValue(resourceProvider.getString(R.string.reminder_canceled_message))
    }

    fun scheduleReminderWork() {
        val inputData = Data.Builder()
                .putString(INTENT_MOVIE_TITLE_KEY, movieData.value!!.title!!)
                .putString(INTENT_MOVIE_ID_KEY, movieData.value!!.imdbID!!)
                .build()

        val releaseDate = Calendar.getInstance()
        releaseDate.time = getDateFromMovie(movieData.value!!.released)!!
        val nowDate = Calendar.getInstance()

        val daysToReleaseDate = TimeUnit.MILLISECONDS.toDays(releaseDate.timeInMillis - nowDate.timeInMillis)

        toastEvent.postValue(resourceProvider.getString(R.string.reminder_set_message, daysToReleaseDate))

        val notificationWork = OneTimeWorkRequest.Builder(AlarmWorker::class.java)
                .setInitialDelay(daysToReleaseDate, TimeUnit.DAYS)
                .setInputData(inputData)
                .addTag(movieData.value!!.imdbID!!)
                .build()

        WorkManager.getInstance()
                .beginUniqueWork(movieData.value!!.imdbID!!, ExistingWorkPolicy.KEEP, notificationWork)
                .enqueue()
    }

    fun getDateFromMovie(releaseDate: String?): Date? {
        val simpleDateFormat = SimpleDateFormat("dd MMM yyyy", Locale.US)
        releaseDate?.let {
            return try {
                simpleDateFormat.parse(releaseDate)
            } catch (e: Exception) {
                null
            }
        }
        return null
    }

    fun shouldShowMenu(movie: Movie): Boolean {
        val releaseDate = getDateFromMovie(movie.released)
        if (releaseDate != null) {
            return releaseDate > Date()
        }
        return false
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
        disposableLiveData.dispose()
    }
}
