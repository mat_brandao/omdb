package com.mat_brandao.omdb.view.movie_search

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.mat_brandao.omdb.R
import com.mat_brandao.omdb.domain.model.Movie
import com.mat_brandao.omdb.domain.util.GenericObjectClickListener
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_movie_layout.view.*

class MovieAdapter(private val listener: GenericObjectClickListener<Movie>) : PagedListAdapter<Movie, RecyclerView.ViewHolder>(object : DiffUtil.ItemCallback<Movie>() {
    override fun areItemsTheSame(oldItem: Movie, newItem: Movie): Boolean {
        return oldItem.imdbID == newItem.imdbID
    }

    override fun areContentsTheSame(oldItem: Movie, newItem: Movie): Boolean {
        return oldItem.title == newItem.title
    }
}) {

    private var loading: Boolean = false

    override fun getItemViewType(position: Int): Int {
        return if (loading && position == currentList!!.size) {
            LOADING_VIEW_TYPE
        } else {
            MOVIE_VIEW_TYPE
        }
    }

    override fun getItemCount(): Int {
        return if (loading) {
            super.getItemCount() + 1
        } else super.getItemCount()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == MOVIE_VIEW_TYPE) {
            MovieViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_movie_layout, parent, false))
        } else {
            LoadingViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_loading_layout, parent, false))
        }
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        if (viewHolder is MovieViewHolder) {
            val movie = getItem(position)
            if (movie != null) {
                try {
                    Picasso.get()
                            .load(movie.poster)
                            .placeholder(R.drawable.poster_placeholder)
                            .error(R.drawable.poster_placeholder)
                            .into(viewHolder.moviePosterImg)
                } catch (e: Exception) {
                }

                viewHolder.movieTitleText.text = movie.title
                viewHolder.runtimeYearText.text = viewHolder.itemView.context.getString(R.string.movie_year_placeholder, movie.year)
                viewHolder.itemView.setOnClickListener { listener.onItemClick(movie) }
            }
        }
    }

    fun setLoading(loading: Boolean) {
        this.loading = loading
        notifyDataSetChanged()
    }

    inner class MovieViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val moviePosterImg = itemView.movie_poster_img
        val movieTitleText = itemView.movie_title_text
        val movieRatingText = itemView.movie_rating_text
        val runtimeYearText = itemView.runtime_year_text
        val directorText = itemView.director_text
        val plotText = itemView.plot_text
    }

    inner class LoadingViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    companion object {
        private val LOADING_VIEW_TYPE = 1
        private val MOVIE_VIEW_TYPE = 2
    }
}
