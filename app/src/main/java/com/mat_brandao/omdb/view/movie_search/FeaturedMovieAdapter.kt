package com.mat_brandao.omdb.view.movie_search

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import androidx.viewpager.widget.PagerAdapter
import com.mat_brandao.omdb.R
import com.mat_brandao.omdb.domain.model.Movie
import com.mat_brandao.omdb.domain.util.GenericObjectClickListener
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_featured_movie.view.*

/**
 * Created by orion on 20/09/17.
 */

class FeaturedMovieAdapter(mContext: Context, private val mList: List<Movie>?, private val mListener: GenericObjectClickListener<Movie>, private val callback: Callback) : PagerAdapter() {
    var mLayoutInflater: LayoutInflater = mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    override fun getCount(): Int {
        return mList?.size ?: 0
    }

    override fun isViewFromObject(view: View, type: Any): Boolean {
        return view === type as FrameLayout
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val movie = mList!![position]
        val itemView = mLayoutInflater.inflate(R.layout.item_featured_movie, container, false)
        itemView.setOnClickListener { mListener.onItemClick(mList[position]) }
        val viewHolder = ViewHolder(itemView)

        try {
            Picasso.get()
                    .load(movie.poster)
                    .into(viewHolder.featuredPosterImg, callback)
        } catch (e: Exception) { }

        viewHolder.featuredTitleText.text = movie.title
        viewHolder.featuredRatingText.text = movie.imdbRating

        container.addView(itemView)
        return itemView
    }

    override fun destroyItem(container: ViewGroup, position: Int, type: Any) {
        container.removeView(type as FrameLayout)
    }

    class ViewHolder(view: View) {
        val featuredTitleText: TextView = view.featured_title_text
        val featuredRatingText: TextView = view.featured_rating_text
        val featuredPosterImg: ImageView = view.featured_poster_img

    }
}
