package com.mat_brandao.omdb.view

import androidx.multidex.MultiDexApplication
import androidx.work.Configuration
import androidx.work.WorkManager
import com.facebook.stetho.Stetho
import com.mat_brandao.omdb.BuildConfig
import com.mat_brandao.omdb.domain.ApplicationComponent
import com.mat_brandao.omdb.domain.DaggerApplicationComponent
import com.mat_brandao.omdb.domain.module.ApiModule
import com.mat_brandao.omdb.domain.module.ApplicationModule


class App : MultiDexApplication() {
    val workTag = "notificationWork"

    lateinit var applicationComponent: ApplicationComponent
        private set

    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.DEBUG) {
            Stetho.initializeWithDefaults(this)
        }

        applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(ApplicationModule(applicationContext))
                .apiModule(ApiModule(BuildConfig.SERVER_URL))
                .build()

        instance = this

        WorkManager.initialize(this, Configuration.Builder().build())
    }

    companion object {
        var instance: App? = null
    }
}
