package com.mat_brandao.omdb.view.movie_detail

import android.app.ProgressDialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.work.WorkManager
import com.mat_brandao.omdb.R
import com.mat_brandao.omdb.domain.model.Movie
import com.mat_brandao.omdb.domain.util.IntentValues
import com.mat_brandao.omdb.domain.util.IntentValues.INTENT_MOVIE_ID_KEY
import com.mat_brandao.omdb.domain.util.NetworkState
import com.mat_brandao.omdb.view.App
import com.mat_brandao.omdb.view.ViewModelFactory
import com.mat_brandao.omdb.view.common.ReminderAction
import com.mat_brandao.omdb.view.common.WorkManagerState
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_movie_detail.*

class MovieDetailActivity : AppCompatActivity() {

    private lateinit var viewModel: MovieDetailViewModel
    private var progressDialog: ProgressDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_detail)

        viewModel = ViewModelProviders.of(this,
                ViewModelFactory(application as App))
                .get(MovieDetailViewModel::class.java)

        toolbar.setNavigationIcon(R.drawable.ic_arrow_back)
        toolbar.setNavigationOnClickListener { finish() }
        toolbar.setOnMenuItemClickListener {
            if (it.itemId == R.id.action_reminder) {
                viewModel.onMenuReminderClick()
                return@setOnMenuItemClickListener true
            }
            return@setOnMenuItemClickListener false
        }

        viewModel.movieData
                .observe(this, Observer { movie ->
                    toolbar.title = movie.title
                    movie_title_text.text = movie.title
                    movie_subtitle_text.text = String.format("%s  %s  %s", movie.year, movie.runtime, movie.genre)
                    try {
                        Picasso.get()
                                .load(movie.poster)
                                .error(R.drawable.poster_placeholder)
                                .into(movie_poster_img)
                    } catch (e: Exception) { }

                    if (movie.plot != null) {
                        movie_plot_text.text = movie.plot
                    }
                    if (movie.director != null) {
                        movie_director_text.visibility = View.VISIBLE
                        movie_director_text.text = String.format("Diretor: %s", movie.director)
                    }
                    if (movie.awards != null) {
                        movie_awards_text.visibility = View.VISIBLE
                        movie_awards_text.text = movie.awards
                    }
                    if (movie.imdbRating != null || movie.metascore != null) {
                        rating_layout.visibility = View.VISIBLE
                        movie_rating_text.setText(movie.imdbRating)
                        movie_metascore_text.setText(movie.metascore)
                    }
                })

        viewModel.networkState
                .observe(this, Observer { networkState ->
                    when (networkState) {
                        NetworkState.SUCCESS -> if (progressDialog != null && progressDialog!!.isShowing) {
                            progressDialog!!.dismiss()
                        }
                        NetworkState.LOADING -> {
                            if (progressDialog == null) {
                                progressDialog = ProgressDialog(this)
                            }
                            progressDialog!!.setMessage("Aguarde")
                            progressDialog!!.show()
                        }
                        NetworkState.ERROR -> {
                            if (progressDialog != null && progressDialog!!.isShowing) {
                                progressDialog!!.dismiss()
                            }
                            Toast.makeText(this, "Verifique sua conexão com a internet", Toast.LENGTH_SHORT).show()
                        }
                    }
                })

        viewModel.workManagerState.observe(this, Observer {
            handleWorkManagerState(it)
        })

        viewModel.reminderAction.observe(this, Observer {
            showReminderDialog(it)
        })

        viewModel.toastEvent.observe(this, Observer {
            Toast.makeText(this, it, Toast.LENGTH_LONG).show()
        })

        if (intent.hasExtra(INTENT_MOVIE_ID_KEY)) {
            viewModel.setupMovieLiveData(Movie(imdbID = intent.getStringExtra(INTENT_MOVIE_ID_KEY)))
        } else {
            viewModel.setupMovieLiveData(intent.getSerializableExtra(IntentValues.INTENT_MOVIE_DATA_KEY) as Movie)
        }
    }

    private fun handleWorkManagerState(state: WorkManagerState) {
        when (state) {
            is WorkManagerState.Subscribe -> {
                WorkManager.getInstance().getWorkInfosByTagLiveData(state.imdbId)
                        .observe(this, Observer { workInfo ->
                            resetMenu()
                            viewModel.onWorkEventReceived(workInfo)
                        })
            }
            is WorkManagerState.WorkInfoState -> {
                state.workState?.let {
                    toolbar.menu.findItem(R.id.action_reminder).icon = ContextCompat.getDrawable(this, R.drawable.ic_alarm_off_white_24dp)
                }
            }
        }
    }

    private fun resetMenu() {
        toolbar.menu?.clear()
        toolbar.inflateMenu(R.menu.reminder_menu)
    }

    private fun showReminderDialog(reminderAction: ReminderAction) {
        val builder = AlertDialog.Builder(this)
                .setTitle(if (reminderAction.isWorkEnqueued) "Remover Lembrete" else "Agendar Lembrete")
                .setMessage(
                        if (reminderAction.isWorkEnqueued) "Deseja remover o lembrete da data de lançamento do filme ${reminderAction.movieTitle}?"
                        else "Deseja agendar um lembrete para a data de lançamento do filme ${reminderAction.movieTitle}?")
                .setPositiveButton(if (reminderAction.isWorkEnqueued) "Remover Lembrete" else "Agendar",
                        if (reminderAction.isWorkEnqueued) { _: DialogInterface, _: Int ->
                            viewModel.cancelScheduledReminderWork()
                        } else { _: DialogInterface, _: Int ->
                            viewModel.scheduleReminderWork()
                        })

        builder.create().show()
    }
}
