package com.mat_brandao.omdb.framework

import com.mat_brandao.omdb.domain.ApplicationComponent
import com.mat_brandao.omdb.domain.module.ApiModule
import com.mat_brandao.omdb.domain.module.ApplicationModule
import com.mat_brandao.omdb.ui.MovieDetailViewModelTest
import com.mat_brandao.omdb.ui.MovieSearchViewModelTest
import com.mat_brandao.omdb.util.ResourceProviderTest
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [
    ApplicationModule::class,
    ApiModule::class])
interface ApplicationComponentTest: ApplicationComponent {
    fun inject(viewModel: MovieSearchViewModelTest)
    fun inject(movieDetailViewModelTest: MovieDetailViewModelTest)
    fun inject(resourceProviderTest: ResourceProviderTest)
}