package com.mat_brandao.omdb.framework

import com.mat_brandao.omdb.domain.MovieService
import com.mat_brandao.omdb.domain.model.Movie
import com.mat_brandao.omdb.domain.model.MovieSearch
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.mock.BehaviorDelegate

class MockMovieService(private val delegate: BehaviorDelegate<MovieService>, private val success: Boolean): MovieService {

    override fun searchMovieList(page: Long?, term: String, type: String, apiKey: String): Observable<Response<MovieSearch>> {
        val movieSearch = MovieSearch()
        if (success) {
            var movie = Movie(poster = "https://m.media-amazon.com/images/M/MV5BZDEyN2NhMjgtMjdhNi00MmNlLWE5YTgtZGE4MzNjMTRlMGEwXkEyXkFqcGdeQXVyNDUyOTg3Njg@._V1_SX300.jpg",
                    year = "2002",
                    type = "movie",
                    imdbID = "tt0145487",
                    title = "Spider-Man")
            var movieList = mutableListOf(movie)

            movieSearch.totalResults = 1
            movieSearch.isResponse = true
            movieSearch.movieList = movieList
        } else {
            movieSearch.error = "Movie not found!"
            movieSearch.isResponse = false
        }
        return delegate.returningResponse(movieSearch).searchMovieList(page, term, type, apiKey)
    }

    override fun getMovieInformation(id: String, apiKey: String): Observable<Response<Movie>> {
        var movie = Movie()
        if (success) {
            movie = Movie(poster = "https://m.media-amazon.com/images/M/MV5BZDEyN2NhMjgtMjdhNi00MmNlLWE5YTgtZGE4MzNjMTRlMGEwXkEyXkFqcGdeQXVyNDUyOTg3Njg@._V1_SX300.jpg",
                    year = "2002",
                    type = "movie",
                    imdbID = "tt0278731",
                    title = "Spider-Man",
                    error = null,
                    isResponse = true)
        } else {
            movie.error = "Movie not found!"
            movie.isResponse = false
        }
        return delegate.returningResponse(movie).getMovieInformation(id, apiKey)
    }

}