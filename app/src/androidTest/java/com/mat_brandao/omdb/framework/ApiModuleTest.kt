package com.mat_brandao.omdb.framework

import com.mat_brandao.omdb.domain.MovieService
import com.mat_brandao.omdb.domain.module.ApiModule
import retrofit2.Retrofit

class ApiModuleTest(apiUrl: String): ApiModule(apiUrl) {
    override val retrofit: Retrofit
        get() = super.retrofit

    override fun getUsersService(retrofit: Retrofit): MovieService {
        return retrofit.create(MovieService::class.java)
    }
}