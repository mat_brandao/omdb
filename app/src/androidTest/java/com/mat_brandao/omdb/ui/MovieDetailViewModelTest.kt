package com.mat_brandao.omdb.ui

import androidx.test.platform.app.InstrumentationRegistry
import com.mat_brandao.omdb.BuildConfig
import com.mat_brandao.omdb.domain.MovieService
import com.mat_brandao.omdb.domain.datasource.MovieDataRepository
import com.mat_brandao.omdb.domain.module.ApiModule
import com.mat_brandao.omdb.domain.module.ApplicationModule
import com.mat_brandao.omdb.framework.ApplicationComponentTest
import com.mat_brandao.omdb.framework.DaggerApplicationComponentTest
import com.mat_brandao.omdb.framework.MockMovieService
import com.mat_brandao.omdb.view.movie_detail.MovieDetailViewModel
import junit.framework.Assert.*
import org.junit.Before
import org.junit.Test
import retrofit2.Retrofit
import retrofit2.mock.MockRetrofit
import retrofit2.mock.NetworkBehavior
import javax.inject.Inject


class MovieDetailViewModelTest {
    private lateinit var viewModel: MovieDetailViewModel
    private lateinit var applicationComponent: ApplicationComponentTest

    @Inject
    lateinit var movieService: MovieService
    @Inject
    lateinit var retrofit: Retrofit

    lateinit var mockRetrofit: MockRetrofit

    @Before
    fun setup() {
        applicationComponent = DaggerApplicationComponentTest.builder()
                .applicationModule(ApplicationModule(InstrumentationRegistry.getInstrumentation().context))
                .apiModule(ApiModule(BuildConfig.SERVER_URL))
                .build()
        applicationComponent.inject(this)

        val behavior = NetworkBehavior.create()

        mockRetrofit = MockRetrofit.Builder(retrofit)
                .networkBehavior(behavior)
                .build()

        viewModel = MovieDetailViewModel()
    }

    @Test
    fun movieService_injectedCorrectly() {
        assertNotNull(movieService)
    }

    @Test
    fun retrofit_injectedCorrectly() {
        assertNotNull(retrofit)
    }

    @Test
    fun getMovieInformation_successReturned() {
        val delegate = mockRetrofit.create(MovieService::class.java)
        val mockService = MockMovieService(delegate, true)

        //Actual Test
        mockService.getMovieInformation("tt0278731", MovieDataRepository.OMDB_API_KEY)
                .subscribe {
                    assertTrue(it.isSuccessful)
                    assertNotNull(it.body())
                    assertNull(it.body()!!.error)
                    assertTrue(it.body()!!.isResponse)
                    assertEquals(it.body()!!.imdbID, "tt0278731")
                }
    }

    @Test
    fun getMovieInformation_notFoundReturned() {
        val delegate = mockRetrofit.create(MovieService::class.java)
        val mockService = MockMovieService(delegate, false)

        //Actual Test
        mockService.getMovieInformation("tt0278731", MovieDataRepository.OMDB_API_KEY)
                .subscribe({
                    assertTrue(it.isSuccessful)
                    assertNotNull(it.body())
                    assertFalse(it.body()!!.isResponse)
                    assertNotNull(it.body()!!.error)
                    assertEquals(it.body()!!.error, "Movie not found!")
                }, {
                })
    }
}