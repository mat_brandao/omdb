package com.mat_brandao.omdb.util

import androidx.test.platform.app.InstrumentationRegistry
import com.mat_brandao.omdb.BuildConfig
import com.mat_brandao.omdb.domain.module.ApiModule
import com.mat_brandao.omdb.domain.module.ApplicationModule
import com.mat_brandao.omdb.domain.module.ResourceProvider
import com.mat_brandao.omdb.framework.ApplicationComponentTest
import com.mat_brandao.omdb.framework.DaggerApplicationComponentTest
import junit.framework.TestCase.assertEquals
import junit.framework.TestCase.assertNotNull
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.mock
import javax.inject.Inject

class ResourceProviderTest {

    private lateinit var applicationComponent: ApplicationComponentTest

    @Inject
    lateinit var resourceProvider: ResourceProvider

    @Before
    fun setup() {
        applicationComponent = DaggerApplicationComponentTest.builder()
                .applicationModule(ApplicationModule(InstrumentationRegistry.getInstrumentation().targetContext))
                .apiModule(ApiModule(BuildConfig.SERVER_URL))
                .build()
        applicationComponent.inject(this)
    }

    @Test
    fun resourceProvider_injectedCorrectly() {
        assertNotNull(resourceProvider)
    }

    @Test
    fun resourceProvider_contextNotNull() {
        assertNotNull(resourceProvider.context)
    }
}