package com.mat_brandao.omdb.ui

import com.mat_brandao.omdb.domain.model.Movie
import com.mat_brandao.omdb.view.movie_detail.MovieDetailViewModel
import junit.framework.Assert.*
import org.junit.Before
import org.junit.Test
import java.text.SimpleDateFormat
import java.util.*

class MovieDetailViewModelUnitTest {

    private lateinit var viewModel: MovieDetailViewModel

    @Before
    fun setup() {
        viewModel = MovieDetailViewModel()
    }

    @Test
    fun viewModel_getDateFromMovie_returnDateCorrectly() {
        val date = viewModel.getDateFromMovie("19 Jul 2019")
        assertNotNull(date)

        val dateCalendar = Calendar.getInstance().apply {
            time = date!!
        }

        assertEquals(dateCalendar.get(Calendar.DAY_OF_MONTH), 19)
        assertEquals(dateCalendar.get(Calendar.MONTH), Calendar.JULY)
        assertEquals(dateCalendar.get(Calendar.YEAR), 2019)
    }

    @Test
    fun viewModel_getDateFromMovie_dateWithWrongFormat() {
        val date = viewModel.getDateFromMovie("19/07/2019")
        assertNull(date)
    }

    @Test
    fun viewModel_getDateFromMovie_dateNull() {
        val date = viewModel.getDateFromMovie(null)
        assertNull(date)
    }

    @Test
    fun viewModel_shouldShowMenu_futureDate() {
        val showMenu = viewModel.shouldShowMenu(Movie(released = "19 Dec 2019"))
        assertTrue(showMenu)
    }

    @Test
    fun viewModel_shouldShowMenu_todayDate() {
        val dateFormat = SimpleDateFormat("dd MMM yyyy")
        val nowDate = dateFormat.format(Calendar.getInstance().time)

        val showMenu = viewModel.shouldShowMenu(Movie(released = nowDate))
        assertFalse(showMenu)
    }

    @Test
    fun viewModel_shouldShowMenu_pastDate() {
        val showMenu = viewModel.shouldShowMenu(Movie(released = "19 Dec 2018"))
        assertFalse(showMenu)
    }
}
